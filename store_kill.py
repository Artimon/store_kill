from subprocess import run
from subprocess import PIPE
from time import sleep


def cmd(arg):
    cmd.result = run(arg, check=True, shell=True, stdout=PIPE)

prog = 'WinStore.App.exe'

while True:
    sleep(0.2)
    cmd('tasklist.exe')
    proc_list = (cmd.result.stdout.decode('utf-8'))
    #print(proc_list)
    if prog in proc_list:
        cmd('taskkill.exe /IM' + ' ' + prog + ' ' + '/F')
        print('found and killed')
    else:
        try:
            print('monitoring...')
        except:
            pass