from subprocess import run
from subprocess import PIPE
from time import sleep


def cmd(arg):
    cmd.result = run(arg, check=True, shell=True, stdout=PIPE)

prog = ['notepad.exe', 'mspaint.exe', 'calc.exe']

while True:
    sleep(0.2)
    cmd('tasklist.exe')
    proc_list = (cmd.result.stdout.decode('utf-8'))
    #print(proc_list)
    for list in prog:
        if list in proc_list:
            cmd('taskkill.exe /IM' + ' ' + list + ' ' + '/F')
            print(list, 'found and killed')
        else:
            try:
                print('monitoring...')
            except:
                pass