**store_kill.py:**

python3 application that uses the subprocess.run module by calling "tasklist.exe" every 0.2 seconds to check and kill the 'WinStore.App.exe' process.

**win_app_kill.py:**

python3 application that uses the subprocess.run module by calling "tasklist.exe" every 0.2 seconds to check and kill 
['notepad.exe', 'mspaint.exe', 'calc.exe', 'CHOOSE_YOUR_APPS'] processes.
line 09: you can add/change the programs you like to monitor and kill by editing the "prog = [CHOOSE_YOUR_APPS]".


**Why I created this project:**

from: https://support.microsoft.com/en-us/help/3135657/can-t-disable-windows-store-in-windows-10-pro-through-group-policy

> **"Can't disable Microsoft Store in Windows 10 Pro through Group Policy**

> Applies to: Windows 10, version 1903, Windows 10, version 1809, Windows 10, version 1511

> **Symptoms**

> On a computer that's running Windows 10 Pro, you upgrade to Windows 10, version 1511, 
Windows 10, version 1809 or Windows 10, version 1903. After the upgrade, you notice that the following Group Policy settings to disable Microsoft Store
are not applied, and you cannot disable Microsoft Store:

> Computer Configuration>Administrative Templates>Windows Components>Store>Turn off the Store application
> User Configuration>Administrative Templates>Windows Components>Store>Turn off the Store

> **Cause**

> **This behavior is by design.** In Windows 10, version 1511, Windows 10, version 1809, and Windows 10, version 1903, these policies are applicable to users of the Enterprise and Education editions only.

> Last Updated: May 21, 2019"



**"This behavior is by design." - seriously?**

You can add Microsoft Windows 10 Pro to Active Directory Server, but you cannot control user access to the Microsoft Store App even with GPO?

The goal of the project was not to eliminate completely the Microsoft Store App.
that you can do with [Windows10Debloater](https://github.com/Sycnex/Windows10Debloater),
but to limit the access from some computers in the company domain.

Create .exe file:
with pyinstaller:
1. pip install pyinstaller
2. Go to your program’s directory and run:
3. pyinstaller yourprogram.py



Create service:
with [NSSM - the Non-Sucking Service Manager](https://nssm.cc)

instruction:
1. nssm.exe install store_kill
2. point to your .exe file, in our case it's store_kill.exe
3. nssm.exe start store_kill

now the service is running and monitoring.

to find more about nssm commands, go to: https://nssm.cc/commands